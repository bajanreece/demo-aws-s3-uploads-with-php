<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Upload a file!</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Upload-a-file!</a>
        </div>
        <div class="navbar-collapse collapse">

        </div><!--/.navbar-collapse -->
      </div>
    </div>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Upload an image</h1>
        <p>This is a super complicated app. Upload a file to get started</p>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <form class="form-inline" action="uploadifier.php" method="post" enctype="multipart/form-data" role="form">
            <div class="form-group">
              <label for="img_name">Add your file</label>
              <input type="file" id="img_name" name="img_name">
              <p class="help-block">Add an image file here (JPEG|PNG).</p>
            </div>
            <button id="upload-image" type="submit" class="btn btn-primary">Upload!</button>
          </form>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12 image_holder"></div>
      </div>
    </div>



    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
      var upload = upload || {};

        upload.process = {
          _list_path      : 'uploadifier.php',
          default_message :  'Hang on, I\'m working on this image',
          files : null,
          opts  : {},
          watchFileUpdates  : function() {
            var self = this;
            $('input[type=file]').on('change', function(e) {
              self.files = e.target.files;
              console.log(": ", self.files);
            });
          },
          watchSendButton   : function() {
              var self = this;
              $('#upload-image').on('click', function(e) {
                e.preventDefault();
                self.send(e);
              });
            },
          getList       : function() {
            var self = this;
            self.opts.list = 1;
            $.ajax({
                  url      : self._list_path,
                  type     : 'GET',
                  dataType : 'json',
                  data     : self.opts,
                  success: function(data) {
                    if(data.length > 0) {
                      data.reverse();
                      for (i = 0; i < data.length; i++) {
                        var imgHTML = '<img src="' + data[i] + '" class="s3_image">';
                        $(".image_holder").append(imgHTML);
                      }

                    }
                  },
                  error : function(xhr, status, err) {
                    console.log('XHR: ', xhr);
                    console.log('Status: ', status);
                    console.log('Err: ', err);
                  }
              });
          }
        };

        document.addEventListener('DOMContentLoaded', function(){
          upload.process.getList();
        });

    </script>
  </body>
</html>
