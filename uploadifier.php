<?php
require 'vendor/autoload.php';
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Doctrine\Common\Cache\FilesystemCache;
use Guzzle\Http\EntityBody;
use Guzzle\Cache\DoctrineCacheAdapter;

// A set of tools to upload images to S3
$bucket_name  = 'hub-101-demo-php-uploads';

// You should have this setup in your Apache Virtual Hosts configuration
if(getenv('APPLICATION_ENV') && getenv('APPLICATION_ENV') == 'local') { // the local test server
  $client  = S3Client::factory(array(
                            'key' => getenv('AWS_CREDENTIALS_o2_DEMO_KEY'),
                            'secret' => getenv('AWS_CREDENTIALS_o2_DEMO_SECRET')
                            ));
} else {
  // from:
  // http://blogs.aws.amazon.com/php/post/Tx1F82CR0ANO3ZI/Providing-credentials-to-the-AWS-SDK-for-PHP
  // Create a cache adapter that stores data on the filesystem
  $cacheAdapter = new DoctrineCacheAdapter(new FilesystemCache('/tmp/cache'));
  // Provide a credentials.cache to cache credentials to the file system
  $client = S3Client::factory(array('credentials.cache' => $cacheAdapter));
}


function uploadFileToBucket($c, $bn, $file_name, $file_path) {
  try {
    $resource = EntityBody::factory(fopen($file_path, 'r+'));
    // $resource = fopen($file_path, 'r');
    $c->upload($bn, $file_name, $resource, 'public-read');
    header("location: index.php?s=1");
    die;
  } catch (S3Exception $e) {
    header("location: index.php?err=1&r=" . urlencode("There was an error $e uploading $file_name to the bucket $bn"));
    die;
  }
}

if(isset($_GET['list'])) {
    $iterator = $client->getIterator('ListObjects', array('Bucket' => $bucket_name));
    $out = array();
    foreach ($iterator as $object) {
      $out[] = $client->getObjectUrl($bucket_name,$object['Key']);
    }
    exit(json_encode($out));
}

if(isset($_FILES['img_name'])) {

  if($_FILES['img_name']['error'] > 0) {
    header("location: index.php?err=1&r=" . urlencode($_FILES['img_name']['error']));
    die;
  }

  $fn = $_FILES['img_name']['name'];
  $fp = $_FILES['img_name']['tmp_name'];
  if(!getimagesize($fp)) {
    header("location: index.php?err=true&r=not_image");
    die;
  } else if (isset($fn, $fp, $client, $bucket_name)) {
    uploadFileToBucket($client, $bucket_name, $fn, $fp);
  }

  exit();
}




